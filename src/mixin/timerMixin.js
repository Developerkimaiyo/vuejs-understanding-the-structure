export const timerMixin = {
  data() {
    return {
      timerCount: 9000
    }
  },
  watch: {
    timerCount: {
      handler(value) {
        if (value > 0) {
          setTimeout(() => {
            this.timerCount--
          }, 1000)
        }
      },
      immediate: true
    }
  }
}
