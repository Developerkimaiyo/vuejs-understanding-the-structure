import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  linkExactActiveClass: 'vue-school-active-class',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      props: true
    },
    {
      path: '/props/:id',
      name: 'props',
      props: true,
      component: () => import(/* webpackChunkName: "Details"*/ './views/Prop.vue')
    }
  ]
})

export default router
