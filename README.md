# VueJs Understanding the Structure


Simple single page application   build with vue that describe the structure of vue

### Link to website 

Link: [https://vue-2-task.web.app](https://vue-2-task.web.app)


 

### Usage

```js
git clone  https://gitlab.com/Developerkimaiyo/vuejs-understanding-the-structure.git



```

### Support

Reach out to me at one of the following places!

- Twitter at <a href="http://twitter.com/maxxmalakwen" target="_blank">`@maxxmalakwen`</a>

Let me know if you have any questions. Email me At maxwell@sendyit.com or developerkimaiyo@gmail.com

---

### License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2021 © <a href="https://github.com/Developer-Kimaiyo" target="_blank">Maxwell Kimaiyo</a>.
